package INF102.lab3.numberGuesser;


public class MyGeniusGuesser implements IGuesser {

    public static void main(String[] args) {
        RandomNumber num = new RandomNumber(1, 100);
        MyGeniusGuesser guesser = new MyGeniusGuesser();

        guesser.findNumber(num);
    }

	@Override
    public int findNumber(RandomNumber number) {
        int upper = number.getUpperbound();
        int lower = number.getLowerbound();
        return makeGuess(number, lower, upper);
    }
    
    private int makeGuess(RandomNumber number, int lower, int upper) {
        int mid = lower + ((upper - lower) / 2);
        int guess = mid;
        int result = number.guess(guess);
        if (result == 0 || lower == upper) {
            return guess;
        }
        if (result == -1) {
            return makeGuess(number, mid + 1, upper);
        }
        else if (result == 1) {
            return makeGuess(number, lower, mid - 1);
        }
        return -1;
    }
}
