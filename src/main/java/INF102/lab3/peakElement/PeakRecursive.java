package INF102.lab3.peakElement;

import java.util.List;

public class PeakRecursive implements IPeak {

    @Override
    public int peakElement(List<Integer> numbers) {
        return findPeak(numbers, 0);
    }

    private int findPeak(List<Integer> numbers, int index) {
        int prev = 0; 
        int next = 0;
        int current = numbers.get(index);
        if (index != 0) {
            prev = numbers.get(index -1);
        }
        if (index != numbers.size() - 1) {
            next = numbers.get(index + 1);
        }
        if (current >= prev && current >= next) {
            return current;
        }
        return findPeak(numbers, index + 1);
    }
}
